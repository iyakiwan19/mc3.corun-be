'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Assessments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      race_category: {
        type: Sequelize.STRING
      },
      level: {
        type: Sequelize.INTEGER
      },
      available_day: {
        type: Sequelize.STRING
      },
      latest_running_date: {
        type: Sequelize.DATEONLY
      },
      latest_running_distance: {
        type: Sequelize.FLOAT
      },
      latest_running_duration: {
        type: Sequelize.TIME
      },
      latest_running_pace: {
        type: Sequelize.TIME
      },
      latest_running_indoor: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Assessments');
  }
};