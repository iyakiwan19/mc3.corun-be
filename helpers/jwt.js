const jwt = require('jsonwebtoken')

function getToken(obj) {
  return jwt.sign({
    id: obj.id,
    uuid: obj.uuid
  }, process.env.JWT_KEY)
}

function verifyToken(token) {
  return jwt.verify(token, process.env.JWT_KEY)
}

module.exports = {
  getToken,
  verifyToken
}