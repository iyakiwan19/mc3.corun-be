const { User } = require('../../models')
const { getToken } = require('../../helpers/jwt')

class UserController {
  static async loginWithApple(req, res) {
    const obj = {
      uuid: req.body.uuid,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      photo: req.body.photo
    }
    try {
      if (!obj.uuid) {
        throw { 
          status: 401,
          message: "Invalid account"
        }
      }
      const [user, created] = await User.findOrCreate({
        where: { uuid: obj.uuid },
        defaults: {
          uuid: obj.uuid,
          first_name: obj.first_name,
          last_name: obj.last_name,
          photo: obj.photo
        }
      })
      const accessToken = getToken(user)
      res.status(200).json({ ...user.dataValues, accessToken })
    } catch (error) {
      if (error.status) {
        res.status(error.status).json({ message: error.message })
      } else {
        res.status(500).json({ message: "there is an error when sign in use apple ID" })
      }
    }
  }
}

module.exports = UserController