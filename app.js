if (process.env.NODE_ENV !== "production") {
  require('dotenv').config()
}
const express = require('express')
const app = express()
const PORT = process.env.PORT
const cors = require('cors')
const { userRoute } = require('./routes')

app.use(cors())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use('/user', userRoute)

app.listen(PORT, () => {
  console.log(`running on port : ${PORT}`)
})