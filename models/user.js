'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.hasMany(models.Assessment)
    }
  }
  User.init({
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    sex: DataTypes.INTEGER,
    birth_date: DataTypes.DATEONLY,
    height: DataTypes.INTEGER,
    weight: DataTypes.INTEGER,
    uuid: {
      type: DataTypes.STRING,
      unique: true
    },
    photo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};