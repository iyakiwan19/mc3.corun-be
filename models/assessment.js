'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Assessment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Assessment.belongsTo(models.User)
    }
  }
  Assessment.init({
    race_category: DataTypes.STRING,
    level: DataTypes.INTEGER,
    available_day: DataTypes.STRING,
    latest_running_date: DataTypes.DATEONLY,
    latest_running_distance: DataTypes.FLOAT,
    latest_running_duration: DataTypes.TIME,
    latest_running_pace: DataTypes.TIME,
    latest_running_indoor: DataTypes.BOOLEAN,
    UserId: DataTypes.INTEGER,
    CoachId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Assessment',
  });
  return Assessment;
};