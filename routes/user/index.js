const userRoute = require('express').Router()
const { UserController } = require('../../controllers')

userRoute.post('/sign-in-apple', UserController.loginWithApple)

module.exports = userRoute